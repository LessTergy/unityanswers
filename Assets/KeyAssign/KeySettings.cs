﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class KeySettings : MonoBehaviour {
    [SerializeField]
    public List<KeyOptionObject> keyOptionUIList = new List<KeyOptionObject>();
    private KeyOptionObject currentKeyObject = null;
    
    // Use this for initialization
    void Start() {
        List<KeyAction> keyList = KeyAction.KeyList;
        foreach (var keyAction in keyList) {
            keyAction.LoadKeyOption();
        }

        foreach (var keyOptionUI in keyOptionUIList) {
            keyOptionUI.button.onClick.AddListener(delegate { ChangeKey(keyOptionUI); });
        }

        UpdateText();
    }

    // Update is called once per frame
    void Update() {

        //Movement Keys
        if (Input.GetKey(KeyAction.Up.Key)) {
            Debug.Log("Forword is pressed");
        }
        if (Input.GetKey(KeyAction.Right.Key)) {
            Debug.Log("Right is pressed");
        }
        if (Input.GetKey(KeyAction.Left.Key)) {
            Debug.Log("Left is pressed");
        }
        if (Input.GetKey(KeyAction.Down.Key)) {
            Debug.Log("Down is pressed");
        }
        if (Input.GetKey(KeyAction.Run.Key)) {
            Debug.Log("Run start");
        } else {
            Debug.Log("Run stop");
        }

        //MenuKeys
        if (Input.GetKeyDown(KeyAction.HideUI.Key)) {
            Debug.Log("HideUI is working");
        }
    }

    private void UpdateText() {
        foreach (var keyActionUI in keyOptionUIList) {
            keyActionUI.UpdateText();
        }
    }

    void OnGUI() {
        if (currentKeyObject != null) {
            Event e = Event.current;
            if (e.isKey) {
                KeyAction keyAction = KeyAction.GetKey(currentKeyObject.keyAction.name);
                keyAction.SetKey(e.keyCode);
                currentKeyObject.UpdateText();
                currentKeyObject.Unselect();
                currentKeyObject = null;
            }
        }
    }

    private void ChangeKey(KeyOptionObject keyObject) {
        if (currentKeyObject != null) {
            currentKeyObject.Unselect();
        }
        currentKeyObject = keyObject;
        currentKeyObject.Select();
    }

    public void SaveKeys() {
        List<KeyAction> keyList = KeyAction.KeyList;
        foreach (var key in keyList) {
            key.SaveKeyOption();
        }
        PlayerPrefs.Save();
    }
}

public sealed class KeyAction {
    private static readonly Dictionary<string, KeyAction> instance = new Dictionary<string, KeyAction>();

    public readonly string name;
    private KeyCode defaultKey;
    public KeyCode Key { get; private set; }

    //List of keys
    public static readonly KeyAction Default = new KeyAction("Default", KeyCode.None);
    public static readonly KeyAction Up = new KeyAction("Up", KeyCode.W);
    public static readonly KeyAction Right = new KeyAction("Right", KeyCode.D);
    public static readonly KeyAction Left = new KeyAction("Left", KeyCode.A);
    public static readonly KeyAction Down = new KeyAction("Down", KeyCode.S);
    public static readonly KeyAction Run = new KeyAction("Run", KeyCode.Q);
    public static readonly KeyAction Jump = new KeyAction("Jump", KeyCode.Space);
    public static readonly KeyAction Interact = new KeyAction("Interact", KeyCode.E);
    public static readonly KeyAction HideUI = new KeyAction("HideUI", KeyCode.Escape);

    //constructor
    private KeyAction(string actionName, KeyCode defaultKey) {
        this.name = actionName;
        this.defaultKey = defaultKey;
        Key = defaultKey;
        instance[actionName] = this;
    }

    public void SetKey(KeyCode keyCode) {
        Key = keyCode;
    }

    //key getters
    public static KeyAction GetKey(string name) {
        if (instance.ContainsKey(name)) {
            return instance[name];
        }
        return null;
    }

    public static KeyAction GetKey(KeyCode keyCode) {
        foreach (var pair in instance) {
            if (pair.Value.Key == keyCode) {
                return pair.Value;
            }
        }
        return null;
    }

    //load from playerPrefs
    public void LoadKeyOption() {
        Key = (KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(name, defaultKey.ToString()));
    }

    public void SaveKeyOption() {
        PlayerPrefs.SetString(name, Key.ToString());
    }

    public static List<KeyAction> KeyList {
        get {
            return instance.Values.ToList();
        }
    }

    public static string[] GetActionNames() {
        return instance.Keys.ToArray();
    }

    public override string ToString() {
        return name;
    }
}