﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyOptionObject : MonoBehaviour {

    public Image image { get; private set; }
    public Button button { get; internal set; }

    [SerializeField]
    private Text actionText;
    [SerializeField]
    private Text keyText;
    
    public KeyAction keyAction {
        get {
            if (string.IsNullOrEmpty(keyActionName)) {
                return KeyAction.Default;
            }
            return KeyAction.GetKey(keyActionName); }
    }

    [SerializeField]
    [HideInInspector]
    private string keyActionName;

    public Color32 normalColor = new Color32(125, 3, 3, 255);
    public Color32 selectedColor = new Color32(239, 116, 36, 255);

    // Use this for initialization
    void Awake () {
        image = GetComponent<Image>();
        button = GetComponent<Button>();
    }

    public void Select() {
        image.color = selectedColor;
    }

    public void Unselect() {
        image.color = normalColor;
    }

    public void UpdateText() {
        actionText.text = keyAction.name;
        keyText.text = keyAction.Key.ToString();
    }

    private void OnValidate() {
        UpdateText();
    }
}
