﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(KeyOptionObject))]
[CanEditMultipleObjects]
public class KeyOptionObjectEditor : Editor {

    int selected = -1;
    private string[] options = KeyAction.GetActionNames();

    SerializedProperty actionName;

    void OnEnable() {
        var myTarget = (KeyOptionObject)target;
        actionName = serializedObject.FindProperty("keyActionName");
        
        selected = Array.IndexOf(options, actionName.stringValue);
    }

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        serializedObject.Update();
        var myTarget = (KeyOptionObject)target;

        int _selected = EditorGUILayout.Popup("Action type", selected, options);
        _selected = Mathf.Max(_selected, 0);

        if (_selected != selected) {
            selected = _selected;
            actionName.stringValue = options[selected];
        }
        serializedObject.ApplyModifiedProperties();
    }
}
