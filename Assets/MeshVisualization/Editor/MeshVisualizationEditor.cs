﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MeshVisualization))]
public class MeshVisualizationEditor : Editor {
    
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
    }

    void OnSceneGUI() {
        DrawMesh();
    }

    private void DrawMesh() {
        var myTarget = (MeshVisualization)target;
        Mesh mesh = myTarget.GetMesh();

        if (mesh == null || mesh.vertexCount == 0 || mesh.triangles.Length != mesh.vertexCount) {
            return;
        }

        Vector3 positionOffset = myTarget.transform.position;

        int triangleCount = mesh.triangles.Length / 3;
        myTarget.selectedTriangleIndex = Mathf.Clamp(myTarget.selectedTriangleIndex, 0, triangleCount - 1);

        List<Vector3> vertices = new List<Vector3>();
        mesh.GetVertices(vertices);

        //draw vertex points and lines which connect verteces
        for (int i = 0; i < triangleCount; i++) {
            if (i != myTarget.selectedTriangleIndex) {
                DrawTriangle(i, mesh.triangles, vertices, positionOffset, myTarget.meshTriangleProp);
            }
        }

        DrawTriangle(myTarget.selectedTriangleIndex, mesh.triangles, vertices, positionOffset, myTarget.selectedTriangleProp);

        myTarget.selectedPointTriangleIndex = Mathf.Clamp(myTarget.selectedPointTriangleIndex, 0, 3);
        MeshTriangle selectedTriangle = MeshTriangle.Init(myTarget.selectedTriangleIndex, mesh.triangles);
        int selectedVertexIndex = selectedTriangle.GetIndex(myTarget.selectedPointTriangleIndex);

        DrawVertex(vertices[selectedVertexIndex] + positionOffset, myTarget.selectedVertexProp.pointSize, myTarget.selectedVertexProp.color);

        //Handles.color = textColor;
        //GUIStyle style = new GUIStyle();
        //style.fontSize = textSize;

        //Handles.Label(vert1, vert1.ToString(), style);
    }

    private void DrawVertex(Vector3 vertex, float pointSize, Color color) {
        Handles.color = color;
        Handles.SphereCap(0, vertex, Quaternion.identity, pointSize);
    }

    private void DrawTriangle(int triangleIndex, int[] triangles, List<Vector3> vertices, Vector3 positionOffset, TriangleVisualProp triangleProp) {
        Handles.color = triangleProp.color;

        int startIndex = triangleIndex * 3;

        int vIndex1 = triangles[startIndex];
        int vIndex2 = triangles[startIndex + 1];
        int vIndex3 = triangles[startIndex + 2];

        Vector3 vertex1 = vertices[vIndex1] + positionOffset;
        Vector3 vertex2 = vertices[vIndex2] + positionOffset;
        Vector3 vertex3 = vertices[vIndex3] + positionOffset;

        DrawVertex(vertex1, triangleProp.pointSize, triangleProp.color);
        DrawVertex(vertex2, triangleProp.pointSize, triangleProp.color);
        DrawVertex(vertex3, triangleProp.pointSize, triangleProp.color);

        Handles.DrawLine(vertex1, vertex2);
        Handles.DrawLine(vertex2, vertex3);
        Handles.DrawLine(vertex3, vertex1);
    }
}
