﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class VertexSide {
    public readonly VertexSideType type;
    public List<CustomVertex> vertices { get; private set; }

    public float mainCoord {
        get {
            return GetCoordBySide(vertices[0], type);
        }
        set {
            //смещаем все вертексы
            foreach (var vert in vertices) {
                SetCoordBySide(value, vert, type);
            }
        }
    }

    public float width {
        get {
            List<CustomVertex> verts = vertices.OrderBy(v => v.vert.position.x).ToList();
            return Mathf.Abs(GetFirst().vert.position.x - GetLast().vert.position.x);
        }
    }

    public float height {
        get {
            List<CustomVertex> verts = vertices.OrderBy(v => v.vert.position.y).ToList();
            return Mathf.Abs(GetFirst().vert.position.y - GetLast().vert.position.y);
        }
    }

    public VertexSide(List<CustomVertex> vertices, VertexSideType type) {
        this.vertices = SortBySide(vertices, type);
        this.type = type;
    }

    private CustomVertex GetFirst() {
        return vertices[0];
    }

    private CustomVertex GetLast() {
        return vertices[vertices.Count - 1];
    }

    public static float GetCoordBySide(CustomVertex cVert, VertexSideType side) {
        if (side == VertexSideType.Left || side == VertexSideType.Right) {
            return cVert.vert.position.x;
        } else if (side == VertexSideType.Top || side == VertexSideType.Bottom) {
            return cVert.vert.position.y;
        }
        return float.NaN;
    }

    public void SetCoordBySide(float value, CustomVertex cVert, VertexSideType side) {
        if (side == VertexSideType.Left || side == VertexSideType.Right) {
            cVert.vert.position.x = value;
        } else if (side == VertexSideType.Top || side == VertexSideType.Bottom) {
            cVert.vert.position.y = value;
        }
    }

    private List<CustomVertex> SortBySide(List<CustomVertex> vertList, VertexSideType side) {
        if (side == VertexSideType.Left || side == VertexSideType.Right) {
            vertList = vertList.OrderBy(v => v.vert.position.x).ToList();

        } else if (side == VertexSideType.Top || side == VertexSideType.Bottom) {
            vertList = vertList.OrderBy(v => v.vert.position.y).ToList();
        }
        return vertList;
    }
}

public enum VertexSideType {
    Left,
    Right,
    Top,
    Bottom
}