﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class VertexRect {
    public List<CustomVertex> vertices { get; private set; }
    public Vector2 pivot { get; private set; }

    public VertexSide left { get; private set; }
    public VertexSide right { get; private set; }
    public VertexSide top { get; private set; }
    public VertexSide bottom { get; private set; }

    public float width {
        get {
            return Mathf.Abs(left.mainCoord - right.mainCoord);
        }
        set {
            SetWidth(value);
        }
    }

    public float height {
        get {
            return Mathf.Abs(top.mainCoord - bottom.mainCoord);
        }
    }

    public VertexRect(List<CustomVertex> vertices, Vector2 pivot) {
        //сортируем вертексы вдоль осей
        left = new VertexSide(GetVerticesForSide(vertices, VertexSideType.Left), VertexSideType.Left);
        right = new VertexSide(GetVerticesForSide(vertices, VertexSideType.Right), VertexSideType.Right);
        top = new VertexSide(GetVerticesForSide(vertices, VertexSideType.Top), VertexSideType.Top);
        bottom = new VertexSide(GetVerticesForSide(vertices, VertexSideType.Bottom), VertexSideType.Bottom);
        
        this.vertices = vertices;
        this.pivot = pivot;
    }

    private List<CustomVertex> GetVerticesForSide(List<CustomVertex> vertList, VertexSideType side) {
        vertList = SortForSide(vertList, side);
        float firstCoord = VertexSide.GetCoordBySide(vertList[0], side);

        List<CustomVertex> sideVertList = new List<CustomVertex>();

        for (int i = 0; i < vertList.Count; i++) {
            CustomVertex vert = vertList[i];
            if (VertexSide.GetCoordBySide(vert, side) == firstCoord) {
                sideVertList.Add(vert);
            } else {
                break;
            }
        }

        return sideVertList;
    }

    private List<CustomVertex> SortForSide(List<CustomVertex> vertList, VertexSideType side) {
        if (side == VertexSideType.Left) {
            vertList = vertList.OrderBy(v => v.vert.position.x).ToList();

        } else if (side == VertexSideType.Right) {
            vertList = vertList.OrderBy(v => -v.vert.position.x).ToList();

        } else if (side == VertexSideType.Top) {
            vertList = vertList.OrderBy(v => v.vert.position.y).ToList();

        } else if (side == VertexSideType.Bottom) {
            vertList = vertList.OrderBy(v => -v.vert.position.y).ToList();
        }
        return vertList;
    }

    private void SetWidth(float value) {
        float delta = value - width;

        float leftDeltaCoord = delta * (1f - (1f - pivot.x));
        float rightDeltaCoord = delta * (1f - pivot.x);

        left.mainCoord -= leftDeltaCoord;
        right.mainCoord += rightDeltaCoord;
    }
}