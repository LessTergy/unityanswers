﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace UnityEngine.UI {
    [RequireComponent(typeof(RectTransform)), RequireComponent(typeof(Graphic)), DisallowMultipleComponent, AddComponentMenu("UI/Flippable")]
    public class UIFlippable : MonoBehaviour, IMeshModifier {

        [SerializeField]
        private bool m_Horizontal = false;
        [SerializeField]
        private bool m_Veritical = false;

        public bool horizontal {
            get {
                return this.m_Horizontal;
            }
            set {
                this.m_Horizontal = value;
                this.GetComponent<Graphic>().SetVerticesDirty();
            }
        }

        public bool vertical {
            get {
                return this.m_Veritical;
            }
            set {
                this.m_Veritical = value;
                this.GetComponent<Graphic>().SetVerticesDirty();
            }
        }

        protected void OnValidate() {
            this.GetComponent<Graphic>().SetVerticesDirty();
        }

        public void ModifyVertices(List<UIVertex> vertexList) {
            RectTransform rectTransform = this.transform as RectTransform;
            Rect rect = rectTransform.rect;

            for (int i = 0; i < vertexList.Count; ++i) {
                UIVertex vertex = vertexList[i];

                float x = vertex.position.x;
                float y = vertex.position.y;

                if (this.m_Horizontal) {
                    x = GetMirrorVertexCoord(x, rect.center.x);
                }
                if (this.m_Veritical) {
                    y = GetMirrorVertexCoord(y, rect.center.y);
                }

                // Modify positions
                vertex.position = new Vector3(x, y, vertex.position.z);

                // Apply
                vertexList[i] = vertex;
            }
        }

        public float GetMirrorVertexCoord(float coord, float rectCenter) {
            float distanceToCenter = rectCenter - coord;
            float distanceToMirrorPoint = distanceToCenter * 2;
            return coord + distanceToMirrorPoint;
        }

        public void ModifyMesh(Mesh mesh) {
        }

        public void ModifyMesh(VertexHelper vertexHelper) {
            List<UIVertex> buffer = new List<UIVertex>();
            vertexHelper.GetUIVertexStream(buffer);
            ModifyVertices(buffer);
            vertexHelper.AddUIVertexTriangleStream(buffer);
        }
    }
}
