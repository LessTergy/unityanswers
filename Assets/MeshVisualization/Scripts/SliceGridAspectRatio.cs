﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

[RequireComponent(typeof(RectTransform)), RequireComponent(typeof(Graphic)), DisallowMultipleComponent, AddComponentMenu("UI/SliceGridAspectRatio")]
public class SliceGridAspectRatio : MonoBehaviour, IMeshModifier {

    [SerializeField]
    private float m_AspectRatio = 0.5f;

    public float aspectRatio {
        get {
            return this.m_AspectRatio;
        }
        set {
            this.m_AspectRatio = value;
            this.GetComponent<Graphic>().SetVerticesDirty();
        }
    }

    protected void OnValidate() {
        this.GetComponent<Graphic>().SetVerticesDirty();
    }

    public void ModifyMesh(Mesh mesh) {

    }

    public void ModifyMesh(VertexHelper vertexHelper) {
        List<UIVertex> vertices = new List<UIVertex>();
        vertexHelper.GetUIVertexStream(vertices);

        bool is9SliceGrid = vertices.Count == 54;
        if (!is9SliceGrid) {
            return;
        }
        
        List<CustomVertex> customVertices = new List<CustomVertex>();
        for (int i = 0; i < vertices.Count; i++) {
            customVertices.Add(new CustomVertex(i, vertices[i]));
        }

        customVertices = customVertices.OrderBy(v => v.vert.position.x).ToList();
        int rectVertCount = vertices.Count / 2;

        List<CustomVertex> leftRectVert = customVertices.Take(rectVertCount).ToList();
        VertexRect leftRect = new VertexRect(leftRectVert, new Vector2(0f, 0.5f));

        List<CustomVertex> rightRectVert = customVertices.Skip(customVertices.Count - rectVertCount).Take(rectVertCount).ToList();
        VertexRect rightRect = new VertexRect(rightRectVert, new Vector2(1f, 0.5f));

        leftRect.width = leftRect.height * aspectRatio;
        rightRect.width = rightRect.height * aspectRatio;

        foreach (var vert in leftRect.vertices) {
            vertexHelper.SetUIVertex(vert.vert, vert.index);
        }
        foreach (var vert in rightRect.vertices) {
            vertexHelper.SetUIVertex(vert.vert, vert.index);
        }
    }
}

public class CustomVertex {
    public readonly int index;
    public UIVertex vert;

    public CustomVertex(int index, UIVertex vert) {
        this.index = index;
        this.vert = vert;
    }
}