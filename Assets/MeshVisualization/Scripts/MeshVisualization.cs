﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class MeshVisualization : MonoBehaviour, IMeshModifier {
    private Mesh uiMesh;

    public int selectedTriangleIndex;
    public int selectedPointTriangleIndex;

    public TriangleVisualProp meshTriangleProp;
    public TriangleVisualProp selectedTriangleProp;
    public VertexVisualProp selectedVertexProp;

    //public Color textColor = Color.black;
    //public int textSize = 22;

    public void ModifyMesh(Mesh mesh) {
        this.uiMesh = mesh;
    }

    public void ModifyMesh(VertexHelper vertexHelper) {
        uiMesh = new Mesh();
        vertexHelper.FillMesh(uiMesh);
    }

    public Mesh GetMesh() {
        Mesh meshForReturn = null;
        if (uiMesh != null) {
            meshForReturn = uiMesh;
        } else {
            MeshFilter meshFilter = GetComponent<MeshFilter>();
            meshForReturn = meshFilter.sharedMesh;
        }
        return meshForReturn;
    }
}

[Serializable]
public class TriangleVisualProp {
    public float pointSize = 0.01f;
    public Color color = Color.black;
}

[Serializable]
public class VertexVisualProp {
    public float pointSize = 0.01f;
    public Color color = Color.black;
}

public class MeshTriangle {
    public int x { get; private set; }
    public int y { get; private set; }
    public int z { get; private set; }

    public static MeshTriangle Init(int index, int[] triangles) {
        MeshTriangle instance = new MeshTriangle();

        int startIndex = index * 3;

        instance.x = triangles[startIndex];
        instance.y = triangles[startIndex + 1];
        instance.z = triangles[startIndex + 2];

        return instance;
    }

    public int GetIndex(int index) {
        index = Mathf.Clamp(index, 0, 2);
        List<int> list = new List<int>() { x, y, z };

        return list[index];
    }
}
